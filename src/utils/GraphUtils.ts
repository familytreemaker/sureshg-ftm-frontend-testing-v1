import { Edge, GraphData, Node, Relationship } from "../ftm-core/types/Types";

export class GraphUtils {
  static nodeSet:  { [key: string]: number };
  static idSet: Set<number> = new Set();

  static assignIds(names: string[]): { [key: string]: number } {
    const result: { [key: string]: number } = {};
    let id = 1;
    for (const str of names) {
      result[str] = id++;
    }
    return result;
  }

  static getNodes(relationships: Relationship[]): Node[] {
    let nodes: Node[] = [];
    let personFromStrings: string[] = relationships.map(x => x.personFrom.name);
    let personToStrings: string[] = relationships.map(x => x.personTo.name);
    let persons: string[] = Array.from(new Set(personFromStrings.concat(personToStrings)));

    this.nodeSet = this.assignIds(persons);
    for (let person of persons) {
      let node: Node = {
        id: this.nodeSet[person],
        label: person,
      };
      nodes.push(node);
    }

    return nodes;
  }

  static getEdges(relationships: Relationship[]): Edge[] {
    return relationships.map(x => {
      let edge: Edge = {
        from: this.nodeSet[x.personFrom.name],
        to: this.nodeSet[x.personTo.name],
      };
      return edge;
    });
  }

  static getGraph(relationships: Relationship[]): GraphData {
    const graph: GraphData = {
      nodes: GraphUtils.getNodes(relationships),
      edges: GraphUtils.getEdges(relationships),
    };
    return graph;
  }
}