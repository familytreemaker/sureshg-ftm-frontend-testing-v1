import React from 'react';

function FtmHeader() {
  return (
    <header>
      <h1>
        Family Tree Maker
      </h1>
    </header>
  );
}

export default FtmHeader;
