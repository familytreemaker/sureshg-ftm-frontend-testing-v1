import React from 'react';
import DescribeAll from './DescribeAll';
import { Person } from './types/Types';

type AddPersonFormProps = {
  people: Person[]
}

type AddPersonFormState = {
  person: Person
}

class AddPersonForm extends React.Component<AddPersonFormProps, AddPersonFormState> {
  constructor(props: any) {
    super(props);
    let person: Person = {
      name: ""
    }
    this.state = { person };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e: React.FormEvent<HTMLInputElement>): void {
    let person: Person = {
      name: e.currentTarget.value
    }
    this.setState({ person });
  }

  handleSubmit(event: React.SyntheticEvent) {
    this.props.people.push(this.state.person);

    let person: Person = { name: "" };
    this.setState({ person });
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <h1> Add a person </h1>
          <label>
            Name:
            <input type="text" value={this.state.person.name} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Add Person" />
        </form>
        <DescribeAll people={this.props.people} />
      </div>
    );
  }
}

export default AddPersonForm;
