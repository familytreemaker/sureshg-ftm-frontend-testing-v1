import React from 'react';
import Graph from 'react-graph-vis';

import { GraphData } from './types/Types';
import '../css/GraphComponent.css';

type GraphComponentProps = {
  graph: GraphData
}

type GraphComponentState = {
}

class GraphComponent extends React.Component<GraphComponentProps, GraphComponentState> {
  render() {
    // define options for the graph
    const options = {
      layout: {
        hierarchical: false,
      },
      height: "500px",
    };

    const graph = {
      nodes: this.props.graph.nodes.map(node => ({ id: node.id, label: node.label })),
      edges: this.props.graph.edges.map(edge => ({ from: edge.from, to: edge.to }))
    };

    return (
      <div id="graph-wrapper" className='graph-wrapper'>
        <Graph graph={graph} options={options} />
      </div>
    );
  }
}

export default GraphComponent;