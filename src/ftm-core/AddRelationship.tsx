import React from 'react';
import DescribeAll from './DescribeAll';
import { Person, Relationship, RelationshipType } from './types/Types';

type AddRelationshipFormProps = {
  relationships: Relationship[]
}

type AddRelationshipFormState = {
  relationships: Relationship[],
  personFrom: string,
  personTo: string,
  relationshipType: number,
}

class AddRelationshipForm extends React.Component<AddRelationshipFormProps, AddRelationshipFormState> {
  constructor(props: any) {
    super(props);

    let pFrom: Person = {
      name: "",
    }
    let pTo: Person = {
      name: "",
    }
    let type: RelationshipType = RelationshipType.ParentOf;

    let relationship: Relationship = {
      personFrom: pFrom,
      personTo: pTo,
      type,
    }

    let relationships: Relationship[] = this.props.relationships;
    this.state = {
      relationships,
      personFrom: "",
      personTo: "",
      relationshipType: RelationshipType.ParentOf,
    }

    this.handlePersonFromChange = this.handlePersonFromChange.bind(this);
    this.handlePersonToChange = this.handlePersonToChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRelationshipTypeChange = this.handleRelationshipTypeChange.bind(this);
  }

  handlePersonFromChange(e: React.FormEvent<HTMLInputElement>): void {
    this.setState({ personFrom: e.currentTarget.value });
  }

  handlePersonToChange(e: React.FormEvent<HTMLInputElement>): void {
    this.setState({ personTo: e.currentTarget.value });
  }

  handleRelationshipTypeChange(e: any): void {
    this.setState({ relationshipType: e.target.value });
  }

  handleSubmit(e: React.SyntheticEvent) {
    // Clear relationship's state 
    let personFrom: Person = {
      name: this.state.personFrom,
    }
    let personTo: Person = {
      name: this.state.personTo,
    }
    let type: RelationshipType = this.state.relationshipType;

    let relationship: Relationship = {
      personFrom,
      personTo,
      type,
    }
    this.props.relationships.push(relationship);
    this.setState({ relationships: this.props.relationships });
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <h1> Add a relationship </h1>
          <label>
            From:
            <input type="text" value={this.state.personFrom} onChange={this.handlePersonFromChange} />
          </label>

          <label>
            To:
            <input type="text" value={this.state.personTo} onChange={this.handlePersonToChange} />
          </label>

          <label>
            Type:
            <select value={this.state.relationshipType} onChange={this.handleRelationshipTypeChange}>
              {
                Object.values(RelationshipType).filter(x => typeof x !== 'number').map(x => {
                  return <option value={x}>{x}</option>
                })
              }
            </select>
          </label>
          <input type="submit" value="Add Relationship" />
        </form>
        <DescribeAll relationships={this.props.relationships} />
      </div>
    );
  }
}

export default AddRelationshipForm;
