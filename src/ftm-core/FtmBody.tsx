import React from 'react';
import AddPersonForm from './AddPersonForm';
import AddRelationshipForm from './AddRelationship';
import { Person, Relationship } from './types/Types';

type FtmBodyProps = {

}

type FtmBodyState = {
  people: Person[],
  relationships: Relationship[]
}

class FtmBody extends React.Component<FtmBodyProps, FtmBodyState> {
  constructor(props: any) {
    super(props);
    let people: Person[] = [];
    let relationships: Relationship[] = [];
    this.state = {
      people,
      relationships,
    }
  }

  render() {
    return (
      <div id="ftm-body">
        <AddPersonForm people={this.state.people} />
        <AddRelationshipForm relationships={this.state.relationships} />
      </div>
    );
  }
}

export default FtmBody;
