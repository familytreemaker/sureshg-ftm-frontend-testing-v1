import React from 'react';
import { GraphUtils } from '../utils/GraphUtils';
import GraphComponent from './GraphComponent';
import { Person, Relationship, RelationshipType } from './types/Types';

type DescribeAllProps = {
  people?: Person[],
  relationships?: Relationship[]
}

type DescribeAllState = {
}

class DescribeAll extends React.Component<DescribeAllProps, DescribeAllState> {
  getAllPersons = (persons: Person[]) => persons.map(x => x.name);
  getRelationshipType = (type: RelationshipType) => type === RelationshipType.ChildOf ? "ChildOf" : "ParentOf";
  getAllRelationsips = (relationships: Relationship[]) => relationships.map(x => [x.personFrom.name, this.getRelationshipType(x.type), x.personTo.name].join("=>"));

  render() {
    if (this.props.people) {
      return (
        <div id="summary">
          <>
            Here's a summary of all people entered so far -
            {JSON.stringify(this.getAllPersons(this.props.people), null, 2)}
          </>
        </div>
      );
    }

    if (this.props.relationships) {
      return (
        <div id="summary">
          <>
            Here's a summary of all relationships entered so far -
            <div>
              <GraphComponent graph={GraphUtils.getGraph(this.props.relationships)} />
            </div>
          </>
        </div>
      );
    }

    return (
      <div id="summary">
        <>
          No data so far
        </>
      </div>
    );
  }
}

export default DescribeAll;
