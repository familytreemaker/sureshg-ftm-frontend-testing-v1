export enum RelationshipType {
  ParentOf,
  ChildOf,
  SpouseOf,
}

export type Relationship = {
  personFrom: Person,
  personTo: Person,
  type: RelationshipType,
}

export type Person = {
  name: string,
}

export interface Node {
  id: number;
  label: string;
}

export interface Edge {
  from: number;
  to: number;
}

export interface GraphData {
  nodes: Node[];
  edges: Edge[];
}