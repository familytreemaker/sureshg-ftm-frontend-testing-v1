import React from 'react';
import FtmBody from './ftm-core/FtmBody';
import FtmFooter from './ftm-footer/FtmFooter';
import FtmHeader from './ftm-header/FtmHeader';

function App() {
  return (
    <div>
      <FtmHeader />
      <FtmBody />
      <FtmFooter />
    </div>
  );
}

export default App;
